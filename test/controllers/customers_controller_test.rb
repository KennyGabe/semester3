require 'test_helper'

class CustomersControllerTest < ActionController::TestCase
  setup do
    @customer = customers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:customers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create customer" do
    assert_difference('Customer.count') do
      post :create, customer: { cust_address: @customer.cust_address, cust_city: @customer.cust_city, cust_email: @customer.cust_email, cust_fname: @customer.cust_fname, cust_lname: @customer.cust_lname, cust_phone: @customer.cust_phone, cust_state: @customer.cust_state, cust_zip: @customer.cust_zip }
    end

    assert_redirected_to customer_path(assigns(:customer))
  end

  test "should show customer" do
    get :show, id: @customer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @customer
    assert_response :success
  end

  test "should update customer" do
    patch :update, id: @customer, customer: { cust_address: @customer.cust_address, cust_city: @customer.cust_city, cust_email: @customer.cust_email, cust_fname: @customer.cust_fname, cust_lname: @customer.cust_lname, cust_phone: @customer.cust_phone, cust_state: @customer.cust_state, cust_zip: @customer.cust_zip }
    assert_redirected_to customer_path(assigns(:customer))
  end

  test "should destroy customer" do
    assert_difference('Customer.count', -1) do
      delete :destroy, id: @customer
    end

    assert_redirected_to customers_path
  end
end
