# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150429023548) do

  create_table "customers", force: :cascade do |t|
    t.string   "cust_fname"
    t.string   "cust_lname"
    t.string   "cust_phone"
    t.string   "cust_email"
    t.string   "cust_address"
    t.string   "cust_city"
    t.string   "cust_state"
    t.string   "cust_zip"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string   "emp_fname"
    t.string   "emp_lname"
    t.string   "emp_phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inventories", force: :cascade do |t|
    t.string   "make"
    t.string   "model"
    t.integer  "year"
    t.string   "color"
    t.string   "transmission"
    t.decimal  "wholesale_price"
    t.decimal  "markup_price"
    t.string   "status"
    t.string   "vin_number"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "quotes", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "employee_id"
    t.integer  "vehicle_id"
    t.string   "final_price"
    t.string   "status"
    t.date     "date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

end
