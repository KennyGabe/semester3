# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#Customer.delete_all
#Employee.delete_all
#Inventory.delete_all
#Quote.delete_all

Customer.create(cust_fname: "Bill",cust_lname: "Mann",cust_phone: "7134062940",cust_email: "bmann@mail.com",cust_address: "46143 Streetsdale Rd",cust_city: "Houston",cust_state: "TX",cust_zip: "77002")
Customer.create(cust_fname: "Mark",cust_lname: "Jefferson",cust_phone: "8321029583",cust_email: "mefferson@ymail.com",cust_address: "34208 Village Dr",cust_city: "Houston",cust_state: "TX",cust_zip: "77080")
Customer.create(cust_fname: "Monica",cust_lname: "Smith",cust_phone: "8321405937",cust_email: "smmmith@outlook.com",cust_address: "294 1st Crossing",cust_city: "Houston",cust_state: "TX",cust_zip: "77077")
Customer.create(cust_fname: "Leif",cust_lname: "Lett",cust_phone: "2812010542",cust_email: "leaflet@envelope.com",cust_address: "0417 Lillian Blvd",cust_city: "Houston",cust_state: "TX",cust_zip: "77713")
Customer.create(cust_fname: "Sandy",cust_lname: "O'Connor",cust_phone: "7131930210",cust_email: "so_connor@sbcglobal.net",cust_address: "4550 Thierer Place",cust_city: "Houston",cust_state: "TX",cust_zip: "77024")
Customer.create(cust_fname: "Ellie",cust_lname: "Edmonds",cust_phone: "2814301482",cust_email: "eledmond@gmail.com",cust_address: "67628 Fallview Circle",cust_city: "Houston",cust_state: "TX",cust_zip: "77005")
Customer.create(cust_fname: "Jeffrey",cust_lname: "Garcia",cust_phone: "8321295027",cust_email: "JayGar@pacific.com",cust_address: "6726 Fremont Rd",cust_city: "Houston",cust_state: "X",cust_zip: "77088")
Customer.create(cust_fname: "Ted",cust_lname: "Ward",cust_phone: "7132950392",cust_email: "teddy@business.com",cust_address: "63 Westend Plaza",cust_city: "Houston",cust_state: "TX",cust_zip: "77083")
Customer.create(cust_fname: "Liam",cust_lname: "Nelson",cust_phone: "2812952183",cust_email: "linelson@school.edu",cust_address: "2313 Marcy St",cust_city: "Houston",cust_state: "TX",cust_zip: "77023")
Customer.create(cust_fname: "Jenny",cust_lname: "Nurse",cust_phone: "7130193475",cust_email: "jenurse@yahoo.com",cust_address: "840 Cascade Dr",cust_city: "Houston",cust_state: "TX",cust_zip: "77055")

Employee.create(emp_fname: "Barbara",emp_lname: "Hall",emp_phone: "7138709323")
Employee.create(emp_fname: "Cynthia",emp_lname: "Ferguson",emp_phone: "2816990439")
Employee.create(emp_fname: "Juan",emp_lname: "Lopez",emp_phone: "7135512183")
Employee.create(emp_fname: "Jean",emp_lname: "Riley",emp_phone: "2813078534")
Employee.create(emp_fname: "Victor",emp_lname: "Carr",emp_phone: "8320537497")

Inventory.create(make: "Ford",model: "Mustang",year: "1999",color: "Blue",transmission: "Manual",wholesale_price: "18000",markup_price: "19800",status: "Available",vin_number: "1FTSW20R59E002275")
Inventory.create(make: "Chevy",model: "Camaro",year: "1998",color: "Yellow",transmission: "Manual",wholesale_price: "21000",markup_price: "23100",status: "Available",vin_number: "1FMFU18505L997097")
Inventory.create(make: "Mini",model: "Cooper",year: "2014",color: "Brown",transmission: "Automatic",wholesale_price: "30000",markup_price: "33000",status: "Available",vin_number: "1G1ZJ57779F197689")
Inventory.create(make: "Chrysler",model: "Crossfire",year: "2007",color: "Grey",transmission: "Manual",wholesale_price: "26000",markup_price: "28600",status: "Available",vin_number: "1FTYR15E44T489507")
Inventory.create(make: "Kia",model: "Forte",year: "2010",color: "Black",transmission: "Automatic",wholesale_price: "23000",markup_price: "25300",status: "Available",vin_number: "KNAFB161225818214")
Inventory.create(make: "Honda",model: "Civic",year: "2015",color: "Beige",transmission: "Automatic",wholesale_price: "18000",markup_price: "19800",status: "Available",vin_number: "1HGES26725L901202")
Inventory.create(make: "Toyota",model: "Corolla",year: "2015",color: "Green",transmission: "Automatic",wholesale_price: "18500",markup_price: "20350",status: "Available",vin_number: "1D4PU4GX5BW158446")
Inventory.create(make: "Subaru",model: "Impreza",year: "1999",color: "Blue",transmission: "Manual",wholesale_price: "33000",markup_price: "36300",status: "Available",vin_number: "2FDFP74W95X441592")
Inventory.create(make: "Mitsubishi",model: "Lancer Evo",year: "2003",color: "Red",transmission: "Manual",wholesale_price: "30000",markup_price: "33000",status: "Available",vin_number: "1FTNF20S61E302086")
Inventory.create(make: "Nissan",model: "GT-R",year: "2016",color: "White",transmission: "Manual",wholesale_price: "111000",markup_price: "122100",status: "Available",vin_number: "1MEFM55262G739405")
Inventory.create(make: "Fiat",model: "500",year: "2013",color: "Red",transmission: "Automatic",wholesale_price: "23000",markup_price: "25300",status: "Available",vin_number: "1B7FL22P51S963473")
Inventory.create(make: "Smart",model: "Pure Coupe",year: "2012",color: "Blue",transmission: "Automatic",wholesale_price: "15000",markup_price: "16500",status: "Available",vin_number: "JTJGF10DX10606104")
Inventory.create(make: "BMW",model: "M5",year: "2011",color: "White",transmission: "Automatic",wholesale_price: "50000",markup_price: "55000",status: "Available",vin_number: "5FNYF3H22BB872628")
Inventory.create(make: "Dodge",model: "Charger",year: "2009",color: "Black",transmission: "Automatic",wholesale_price: "35000",markup_price: "38500",status: "Available",vin_number: "JHLRD788X5C272921")
Inventory.create(make: "Lexus",model: "RC",year: "2014",color: "Silver",transmission: "Automatic",wholesale_price: "57000",markup_price: "62700",status: "Available",vin_number: "1GYS3HEF1BR034226")
Inventory.create(make: "Mazda",model: "3",year: "2008",color: "Grey",transmission: "Manual",wholesale_price: "27000",markup_price: "29700",status: "Available",vin_number: "1FMZU62E0YU179919")
Inventory.create(make: "Hyundai",model: "Veloster",year: "2014",color: "Orange",transmission: "Automatic",wholesale_price: "29000",markup_price: "31900",status: "Available",vin_number: "1GCDS198X48110703")
