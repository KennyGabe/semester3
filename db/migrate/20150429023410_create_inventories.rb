class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.string :make
      t.string :model
      t.integer :year
      t.string :color
      t.string :transmission
      t.decimal :wholesale_price
      t.decimal :markup_price
      t.string :status
      t.string :vin_number

      t.timestamps null: false
    end
  end
end
