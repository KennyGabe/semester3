class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :cust_fname
      t.string :cust_lname
      t.string :cust_phone
      t.string :cust_email
      t.string :cust_address
      t.string :cust_city
      t.string :cust_state
      t.string :cust_zip

      t.timestamps null: false
    end
  end
end
