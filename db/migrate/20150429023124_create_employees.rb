class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :emp_fname
      t.string :emp_lname
      t.string :emp_phone

      t.timestamps null: false
    end
  end
end
