class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :customer_id
      t.integer :employee_id
      t.integer :vehicle_id
      t.string :final_price
      t.string :status
      t.date :date

      t.timestamps null: false
    end
  end
end
