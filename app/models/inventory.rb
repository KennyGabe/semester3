class Inventory < ActiveRecord::Base
  has_many :quotes

  def vehicle
    "#{color} | #{make} | #{model} | VIN: #{vin_number}"
  end

  validates :vin_number, :presence => true
  validates :year, :presence => true

  def markup
    self.wholesale_price.to_s.to_d * (1.10)
  end

  def tax_price
    self.markup_price.to_s.to_d * (1.043)
  end

  def self.search(search)
    where ['color LIKE ? OR model LIKE ? OR vin_number LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%" ]
  end
end