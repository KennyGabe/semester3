class Customer < ActiveRecord::Base
  has_many :quotes

  def cust_name
    "#{cust_lname}, #{cust_fname}"
  end

  def self.search(search)
    where ['cust_lname LIKE ? OR cust_fname LIKE ?', "%#{search}%", "%#{search}%" ]
  end
end