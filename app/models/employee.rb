class Employee < ActiveRecord::Base
  has_many :quotes

  def emp_name
    "#{emp_lname}, #{emp_fname}"
  end

  def self.search(search)
    where ['emp_lname LIKE ? OR emp_fname LIKE ?', "%#{search}%", "%#{search}%" ]
  end
end