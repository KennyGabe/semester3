json.array!(@employees) do |employee|
  json.extract! employee, :id, :emp_fname, :emp_lname, :emp_phone
  json.url employee_url(employee, format: :json)
end
