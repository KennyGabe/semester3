json.extract! @inventory, :id, :make, :model, :year, :color, :transmission, :wholesale_price, :markup_price, :tax_price, :status, :vin_number, :created_at, :updated_at
