json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :make, :model, :year, :color, :transmission, :wholesale_price, :markup_price, :tax_price, :status, :vin_number
  json.url inventory_url(inventory, format: :json)
end
