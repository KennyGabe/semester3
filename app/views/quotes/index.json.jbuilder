json.array!(@quotes) do |quote|
  json.extract! quote, :id, :customer_id, :employee_id, :vehicle_id, :final_price, :status, :date
  json.url quote_url(quote, format: :json)
end
