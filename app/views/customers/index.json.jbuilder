json.array!(@customers) do |customer|
  json.extract! customer, :id, :cust_fname, :cust_lname, :cust_phone, :cust_email, :cust_address, :cust_city, :cust_state, :cust_zip
  json.url customer_url(customer, format: :json)
end
